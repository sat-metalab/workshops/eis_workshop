#!/bin/bash

source_directory="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
eis_directory="${HOME}/src/EditionInSitu"
project_name="$(basename $(pwd))"
execution_options=""
option_c="${source_directory}/eis_config.json"
option_s="${source_directory}/assets/export/gltf/${project_name}.gltf"

for i in "$@"; do
    case $i in
	      -c)
	          echo -e "Hello\n\tOption $i is already being used by default ($option_c).\n\tBut if you really want this, you may have to modify this script to suit\n\tyour needs"
            exit 1
            ;;
	      -s)
	          echo -e "Hello\n\tOption $i is already being used by default ($option_s).\n\tBut if you really want this, you may have to modify this script to suit\n\tyour needs"
            exit 1
            ;;
	      *)
	          execution_options="${execution_options} $i"
	          ;;
    esac
done

cd ${eis_directory}/bin
# PYTHONOPTIMIZE=1 ./eisserver ${execution_options} -s "$option_s"  -c "$option_c"
./eisserver ${execution_options} -c "$option_c"
